import { Cliente } from '../modelo/cliente';

export const CLIENTES: Cliente[] = [
    { id: 1, nombre: 'Javier', nif: '12345678Z' },
    { id: 2, nombre: 'Pepe', nif: 'Y2345678Z' },
    { id: 3, nombre: 'Pedro', nif: '87654321A'}
];
