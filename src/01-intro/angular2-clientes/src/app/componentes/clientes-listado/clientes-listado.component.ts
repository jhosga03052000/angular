import { Component, OnInit } from "@angular/core";
import { ClientesService } from "src/app/servicios/clientes.service";
import { Cliente } from "src/app/modelo/cliente";
import { Observable } from "rxjs";

@Component({
  selector: "app-clientes-listado",
  templateUrl: "./clientes-listado.component.html",
  styleUrls: ["./clientes-listado.component.css"]
})
export class ClientesListadoComponent implements OnInit {
  clientes: Cliente[];

  constructor(private clientesService: ClientesService) {}

  ngOnInit() {
    this.clientesService
      .getClientes()
      .subscribe(
        (clientesRecibidos: Cliente[]) => (this.clientes = clientesRecibidos)
      );
  }

  deleteClient(cliente: Cliente): void {
    console.log("borrando cliente");
    // TODO
    //this.clientesService.deleteClientes(cliente.id).subscribe(clienteBorrado: cliente);
    this.clientesService.deleteClientes(cliente.id).subscribe(
      idRecibido => this.clientes = this.clientes.filter(
        cliente => cliente.id !== idRecibido)
    )
  }
}
