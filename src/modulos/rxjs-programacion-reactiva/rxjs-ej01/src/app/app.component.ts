import { Component } from "@angular/core";
import { interval } from "rxjs";
import { fromEvent } from "rxjs";
import { map } from "rxjs/operators";
import { of } from "rxjs";
import { Observable } from "rxjs";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "rxjs-ej01";

  /**
   * Crear un observable de un contador secuencial cada intervalo de tiempo en RxJS
   */
  observableFromCounterInterval(): void {
    const secondsCounter = interval(1000);

    secondsCounter.subscribe(n =>
      console.log(`It's been ${n} seconds since subscribing!`)
    );
  }

  //------------------------------------------------------------------------------------

  /**
   * Observable de un evento del UI con fromEvent de RxJS
   */
  observableFromEvent(): void {
    const el = document.getElementById("my-element");
    const mouseMoves = fromEvent(el, "mousemove");

    const subscription = mouseMoves.subscribe((evt: MouseEvent) => {
      console.log(`Coords: ${evt.clientX} X ${evt.clientY}`);
    });

    setTimeout(() => {
      subscription.unsubscribe();
    }, 10000);
  }

  //------------------------------------------------------------------------------------

  observableFromEventClick(): void {
    // La forma tradicional de registrar un manejador de eventos
    document.addEventListener("click", () => console.log("Clicked!"));
    // Usando RxJS creamos un observable
    fromEvent(document, "click").subscribe(() => console.log("Clicked!"));
  }

  //------------------------------------------------------------------------------------

  /**
   * Operación sobre un observable, se aplica una función sobre cada elmento de la colección
   */
  observableOperatorMap(): void {
    const nums = of(1, 2, 3);

    const squareValues = map((val: number) => val * val);
    const squaredNums = squareValues(nums);

    squaredNums.subscribe(x => console.log(x));
  }

  //------------------------------------------------------------------------------------

  /**
   *
   */
  observableSimpleIterator(): void {
    const simpleIterator = data => {
      let cursor = 0;
      return {
        next: () => (cursor < data.length ? data[cursor++] : false)
      };
    };

    var consumer = simpleIterator(["simple", "data", "iterator"]);
    console.log(consumer.next()); // 'simple'
    console.log(consumer.next()); // 'data'
    console.log(consumer.next()); // 'iterator'
    console.log(consumer.next()); // false
  }

  //------------------------------------------------------------------------------------

  observableFunction(): void {
    console.log("observableFunction");

    const foo = new Observable(subscriber => {
      console.log("Hello");
      subscriber.next(42);
      subscriber.next(100);
    });

    console.log("Before subscribe");
    foo.subscribe(x => {
      console.log(x);
    });
    console.log("After subscribe");

    console.log("Before subscribe");
    foo.subscribe(y => {
      console.log(y);
    });
    console.log("After subscribe");
  }

  //------------------------------------------------------------------------------------

  observableAsyncFunction(): void {
    const foo = new Observable(subscriber => {
      console.log("Hello");
      subscriber.next(42);
      subscriber.next(100);
      subscriber.next(200);
      setTimeout(() => {
        subscriber.next(300); // happens asynchronously
      }, 1000);
    });

    console.log("before");
    foo.subscribe(x => {
      console.log(x);
    });
    console.log("after");
  }

  //------------------------------------------------------------------------------------

  ngOnInit() {
    //this.observableFromCounterInterval();

    //this.observableFromEvent();
    this.observableFromEventClick();

    //this.observableOperatorMap();

    //this.observableSimpleIterator();

    //this.observableFunction();

    //this.observableAsyncFunction();
  }
}
