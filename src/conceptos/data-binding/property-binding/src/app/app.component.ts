import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  template: `
    <img id="miImagen" [src]="miImagen" />

    <button [disabled]="isDisabled">Cancel is disabled</button>

    <button disabled>HTML Cancel is disabled</button>
  `
})
export class AppComponent {
  title = "property-binding";
  miImagen = "assets/img/keepcalm.PNG";
  isDisabled = true;

  constructor() {
    setTimeout(() => {
      this.isDisabled = false;
    }, 3000);
  }
}
