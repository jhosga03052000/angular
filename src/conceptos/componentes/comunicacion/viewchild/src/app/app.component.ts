import { Component } from "@angular/core";
import { ViewChild, AfterViewInit } from "@angular/core"; // <--- Nuevo
import { ChildComponent } from "./child/child.component"; // <--- Importamos componente anidado

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements AfterViewInit {
  // Añadimos interface AfterViewInit
  title = "viewchild";

  @ViewChild(ChildComponent) child; // <---  Decorador con clase ChildComponent

  constructor() {
    console.log("AppComponent::constructor");
  }

  message: string;

  ngOnInit(): void {
    console.log("AppComponent::ngOnInit");
  }

  /**
   * Capturamos hook cuando componente hijo está cargado
   */
  ngAfterViewInit() {
    console.log("AppComponent::ngAfterViewInit. init");
    this.message = this.child.message;
  }
}
