import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "outputeventemitter";

  message: string; // <--- Nuevo atributo

  /**
   * Función que recibe evento del componente anidado
   * @param $event
   */
  receiveMessage($event) {
    this.message = $event;
  }
}
