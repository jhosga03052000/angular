[TOC]

![](img/primeng-2.png)

## Creación del proyecto

```ts
ng new primeng-basic
```

## Añadiendo PrimeNG y dependencias

Dentro de la carpeta del proyecto instalamos con NPM el paquete de PrimeNG y sus dependencias:

```ts
npm install primeng --save
npm install primeicons --save
```

EN la carpeta del proyecto en la subcarpeta "node_modules" deberíamos encontrar las carpetas "primeng" y "[primeicons](https://www.npmjs.com/package/primeicons)".

Edito "angular.json" y añado las nuevas líneas de abajo:

```ts
            "styles": [
              //"src/styles.css",
              "node_modules/primeng/resources/themes/nova-light/theme.css",
              "node_modules/primeng/resources/primeng.min.css",
              "node_modules/primeicons/primeicons.css"
            ],
```

## ¡Para muestra un botón!

Importo "ButtonModule" en app.module.ts:

```ts
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";

import { ButtonModule } from "primeng/button"; // <----

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, ButtonModule], // <----
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

## Vista

Edito la vista y borro el contenido por defecto para añadir el botón:

```ts
<p-button label="Click" (onClick)="handleClick($event)"></p-button>
{{ clickMessage }}
```

Uso el [_event binding_](https://angular.io/guide/template-syntax#event-binding) de **Angular** para responder al [evento del DOM](https://developer.mozilla.org/en-US/docs/Web/Events) y manejar el evento cuando el usuario hace clic en el botón.

## Componente

Y el código para capturar el evento en "app.component.ts":

```ts
export class AppComponent {
  clickMessage = "";

  handleClick() {
    console.log("handleClick");
    this.clickMessage = "Has hecho clic en botón";
  }
}
```

## Flex Grid: Rejilla responsiva

[Flex Grid](https://www.primefaces.org/primeng/#/flexgrid) CSS permite crear una rejilla para ubicar los elementos de nuestra página.

En la carpeta del proyecto:

```ts
npm install primeflex --save
```

Ahora editamos y añadimos en angular.json en la sección styles:

```ts
"node_modules/primeflex/primeflex.css";
```

## Input

El input [password](https://www.primefaces.org/primeng/#/password) muestra un recuadro informativo que nos indica la fortaleza de la clave que está introduciendo el usuario. Como en los casos anteriores primero debemos importar el módulo "PasswordModule" en app.module.ts.

Ahora sólo tenemos que añadir la etiqueta `input` como sigue para comprobar su funcionamiento:

```ts
<input type="password" pPassword />
```

## Tablas

En algún momento añadiendo el módulo para crear tablas angular-cli ha fallado, después de instalar [@angular/cdk](https://www.npmjs.com/package/@angular/cdk) el problema persistía, la solución parece instalar la versión 7.0.0.

```ts
npm install @angular/cdk@7.0.0
```

## Formulario básico

## Ejemplo: Formulario reactivo

## Código ejemplo

- angular / src / ui / primeng / [primeng-intro](https://gitlab.com/soka/angular/tree/master/src/ui/primeng/primeng-intro).

## Enlaces externos

- Kimserey's blog ["Inline form Angular and PrimeNg"](https://kimsereyblog.blogspot.com/2017/08/inline-form-angular-and-primeng.html).
- medium.com ["PrimeNG porque no todo es material"](https://medium.com/@jorgeucano/primeng-porque-no-todo-es-material-9bb22b73169d) Jorge Cano.
- codeburst.io ["Why not PrimeNG"](https://codeburst.io/why-not-primeng-852a9dfca6bc) Armen Vardanyan.
