[TOC]

# Formulario controlado por plantillas y diseño con Angular Material


# Formularios reactivos VS controlados por plantillas

Los formularios dirigidos por plantillas usan FormsModule, mientras que los formularios reactivos usan ReactiveFormsModule.

Los formularios controlados por plantillas son asíncronos, mientras que los formularios reactivos son sincrónicos.

En los formularios controlados por plantillas, la mayor parte de la interacción se produce en la plantilla, mientras que en los formularios controlados por reactivos, la mayor parte de la interacción se produce en el componente.

# Angular Material



https://material.angular.io/
https://medium.com/@sandy.e.veliz/angular-material-design-instalaci%C3%B3n-angular-material-790caca5677b
https://academia-binaria.com/Material-Design-y-CLI-de-Angular/

## Instalación

https://material.angular.io/guide/getting-started   

Se puede instalar Angular Material de varias formas, usando el gestor de paquetes NPM o con Angular CLI con `ng add`.

```ts
npm install --save @angular/material @angular/cdk @angular/animations
```

```ts
ng add @angular/material
```


# Enlaces internos

# Enlaces externos