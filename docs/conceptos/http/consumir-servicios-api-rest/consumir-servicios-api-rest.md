[TOC]

![](img/chucknorris_logo_coloured_small@2x.png)

# Bienvenido al mundo de las frases de chucknorris.io

El sitio [chucknorris.io](https://api.chucknorris.io/) implementa una **API JSON** en modo lectura que sirve citas satíricas del celebre personaje (ficticias pero desternillantes).

Una llamada GET a [https://api.chucknorris.io/jokes/random](https://api.chucknorris.io/jokes/random) retorna una frase aleatoria, aquí van algunas perlas:

> "Gloria Gaynor: I Will Survive Chuck Norris's Version: They Won't Survive"

> "Chuck Norris can eat an apple through a tennis racket"

> "Chuck Norris can't use Twitter, because he is doing everything, everywhere, all the time. It would break the Internet."

> "Chuck Norris was never born; he just decided to exist."

Si refrescamos la barra del navegador sigue "escupiendo" nuevas frases, perfecto para procrastinar una tarde cualquiera.

El contenido completo de la llamada GET es bastante sencillo:

```ps
StatusCode        : 200
StatusDescription : OK
Content           : {"category":null,"icon_url":"https:\/\/assets.chucknorris.host\/img\/avatar\/chuck-norris.png","id":"KVi1Y2CkTwKX0sI82Jl8og","url":"https:\/\/api.chucknorris.io\/jokes\/KV
                    i1Y2CkTwKX0sI82Jl8og","value"...
RawContent        : HTTP/1.1 200 OK
                    Transfer-Encoding: chunked
                    Connection: keep-alive
                    Access-Control-Allow-Origin: *
                    Access-Control-Allow-Credentials: true
                    Access-Control-Allow-Methods: GET, HEAD
                    Access-Control-All...
Forms             : {}
Headers           : {[Transfer-Encoding, chunked], [Connection, keep-alive], [Access-Control-Allow-Origin, *], [Access-Control-Allow-Credentials, true]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 301
```

El contenido devuelto tiene sintaxis JSON y define cuatro parámetros, una URL a una imagen, un identificador de la cita, otra URL (el ID con la URL para recuperar esta frase concreta) y el _value_ o la propia cita.

```ps
{
"icon_url" : "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
"id" : "1u71CpbnS6a5viIxeGJUbg",
"url" : "https://api.chucknorris.io/jokes/1u71CpbnS6a5viIxeGJUbg"
"value" : "The fall of Berlin wall was caused by Chuck Norris fart."
}
```

La API tiene algunas opciones más pero no es necesario comentarlas ahora.

# Cliente REST HTTP en Angular

![](img/chuck-baila.gif)

Sin mayor dilación empecemos.

Resumen general del los objetivos del proyecto:

- Integrar el **módulo HttpClient** para realizar una llamada GET y obtener un cita del gran Chuck.
- Crear un **servicio** para integrar las llamadas a la API.
- Diseñar el **modelo de datos** para almacenar el contenido devuelto.
- Definir una **estructura de proyecto escalable** y hacer uso de los **módulos** de Angular.
- Programación reactiva con RxJS y **observables** de las llamadas HTTP.

Más adelante se irá explicando pero esta es la estructura del proyecto:

![](img/01.PNG)

# Módulos en Angular (NgModule)

Las apps **Angular** son modulares, los módulos en **Angular** representan una agrupación lógica de componentes y elementos relacionados entre sí funcionalmente.

Todas las apps en **Angular** tienen como mínimo una clase `@NgModule`, [src/app/app.module.ts] contiene la definición del módulo raíz que se llama `AppModule`, este a su vez puede hacer referencia a módulos hijo de forma jerárquica y con múltiples niveles de anidación unos dentro de otros.

Enlaces sobre módulos:

- angular.io ["Introduction to modules"](https://angular.io/guide/architecture-modules).
- angular.io ["NgModules"](https://angular.io/guide/ngmodules).
- ["Base para una aplicación Angular"](https://academia-binaria.com/base-aplicacion-angular/).
- ["Trabajar con módulos en Angular"](https://desarrolloweb.com/articulos/trabajar-modulos-angular.html).
- ["Qué son los Módulos en Angular"](https://rubensa.wordpress.com/2017/10/23/angular-modules/).
- ["Práctica Angular con Módulos, Componentes y Servicios"](https://desarrolloweb.com/articulos/practica-angular-modulos-componentes-servicios.html).

## Descripción del metadata de un módulo

Un módulo está definido por una clase decorada con `@NgModule()`. El decorador `@NgModule()` es una función que recibe un objeto con el metadata. Algunas de sus propiedades:

- `declarations`: Los componentes, directivas y _pipes_ que forman parte de este módulo.
- `exports`: Define los elementos que son visibles y usables en las plantillas de componentes de otros módulos.
- `imports`: Otros módulos que son necesarios que son necesarios aquí.
- `providers`: Servicios que aporta este módulo a la colección global de servicios.
- `bootstrap`: La vista o componente raíz de nuestra app que contiene el resto de vistas.

Este el código de [src/app/app.module.ts] recién creado el proyecto:

```ts
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule], 
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

## Crear un módulo

Una vez lanzado este comando en nuestro proyecto, dentro de la carpeta [src/app] se crea un directorio con el mismo nombre del módulo generado. Dentro encontraremos además el archivo con el código del módulo.

```ps
ng generate module chuck
```

El resultado es la creación del fichero chuck/chuck.module.ts con la declaración y decoración del módulo ChuckModule.

## Configuración del módulo

Para poder usar el componente en el resto de la aplicación agrego al `exports` el componente que quiero usar desde otros módulos.

[src/app/chuck/chuck.module.ts]

```ts
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { VerfraseComponent } from "./componentes/verfrase/verfrase.component"; // <----

@NgModule({
  declarations: [VerfraseComponent],
  imports: [CommonModule],
  exports: [VerfraseComponent] // <----- Componente para visualizar la frase
})
export class ChuckModule {}
```

## Importar el nuevo módulo en la aplicación principal

En el módulo raíz [src/app/app.module.ts] a su vez debo importarlo.

```ts
import { ChuckModule } from "./chuck/chuck.module"; //<------
```

Y en el decorador `@NgModule`:

```ts
imports: [BrowserModule, AppRoutingModule, HttpClientModule, ChuckModule], //<-----
```

# Modelo de datos

El modelo de datos contiene las clases necesarias para representar los datos recibidos en las llamadas a la API, en este ejemplo concreto sólo contiene un [**interfaz TypeScript**](https://www.typescriptlang.org/docs/handbook/interfaces.html) donde almacenamos la frase recibida y el resto de campos, en un futuro si se ampliase la información que puede proveer la API podemos añadir nuevos modelos de datos.

Creo la clase TypeScript usando el siguiente comando:

```ps
ng generate class chuck/modelos/frase
```

Defino la interfaz con sus atributos:

```ts
export interface Frase {
  value: string; // Contiene la frase
  icon_url: string;
  id: string; // Identificador la frase aleatoria
  url: string; // URL que contiene el ID y permite recuperar la frase
}
```

# HttpClient

**Angular** proporciona el módulo [HttpClient](https://angular.io/guide/http) para realizar llamadas HTTP (`@angular/common/http`). Algunas de sus características: API RxJS `Observable`, devuelve objetos con tipo para las peticiones o respuestas, gestión de errores, etc.

Para configurar el nuevo módulo añadimos en [src/app/app.module.ts]:

```ts
import { HttpClientModule } from "@angular/common/http";
```

Más adelante en el servicio usaré el método `get` de la clase `HttpClient` para que Chuck me de una frase hilarante.

Enlaces sobre HttpClient:

- ["Angular 7 HttpClient & Http Services to Consume RESTful API - positronX"](https://www.positronx.io/angular-7-httpclient-http-service/).
- ["Angular HTTP Client - QuickStart Guide"](https://blog.angular-university.io/angular-http/).
- ["Angular 6 HttpClient: Consume RESTful API Example"](https://trello.com/c/CSOYzZyq/21-angular-6-httpclient-consume-restful-api-example).
- ["Comunicaciones http en Angular | Academia Binaria"](https://academia-binaria.com/comunicaciones-http-en-Angular/).
- ["Hacer peticiones con fetch de ECMAScript 6"](https://www.returngis.net/2019/01/hacer-peticiones-con-fetch-de-ecmascript-6/).
- ["Angular 6|7: HttpClient—Building a Service for Sending API Calls and Fetching Data"](https://www.techiediaries.com/angular-httpclient/).
- ["Angular architecture patterns – Additional application features"](https://netmedia.io/dev/angular-architecture-patterns-additional-application-features_5670).

# Crear un servicio para encapsular las llamadas a la API

Lo ideal es usar un servicio para centralizar las llamadas al servidor, de esta forma el resto de la aplicación se abstrae del origen de datos y basta con modificar o ampliar el servicio para adaptar la aplicación si se producen por ejemplo cambios en el protocolo de llamadas.

```ps
ng g service chuck/servicios/api
```

```ts
import { Injectable } from "@angular/core";
import { Frase } from "../modelos/frase";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  private frase: Frase = { value: "", icon_url: "", id: "", url: "" };
  private ChuckUrl = "https://api.chucknorris.io/jokes/random"; // URL to web api

  constructor(private http: HttpClient) {}

  public getFrase(): Observable<Frase> {
    return this.http.get<Frase>(this.ChuckUrl);
  }
}
```

# Librería RxJS y observables en Angular

Los métodos **observables** permiten intercambiar mensajes entre un publicador y los suscriptores, son de especial interés para la gestión de eventos y la programación asíncrona.

Los métodos observables son funciones **declarativas**, se definen para publicar valores pero no se ejecutan hasta que un consumidor se suscribe a ellas, desde ese momento recibe notificaciones hasta que finalice la función observable o finalice la suscripción de forma programada.

**RxJS** es una librería para la programación **reactiva** para programar de forma asíncrona o código basado en [_callbacks_](<https://es.wikipedia.org/wiki/Callback_(inform%C3%A1tica)>) (un puntero a una función que se pasa como argumento a otra función).

Enlaces:

- ["Observables - Angular"](https://angular.io/guide/observables).
- ["The RxJS library - Angular"](https://angular.io/guide/rx-library#the-rxjs-library).

## Instalación

En el raíz del proyecto instalamos el paquete [rxjs-compat](https://www.npmjs.com/package/rxjs-compat), la adaptación de [ReactiveX](http://reactivex.io/) para JavaScript.

```ps
npm install --save rxjs-compat
```

## Observable

Importamos la clase `Observable` en la cabecera de [src/app/chuck/servicios/api.service.ts]:

```ts
import { Observable } from "rxjs";
```

Declaramos un nuevo método `getFrase()` que retorna un objeto de tipo `Frase` y es observable. En el cuerpo de la función realizamos la llamada GET al servidor con la URL como parámetro de la función `HttpClient.get()` también de tipo observable.

```ts
  public getFrase(): Observable<Frase> {
    return this.http.get<Frase>(this.ChuckUrl);
  }
```

## Suscriptor

# Componente con la vista

Creo un nuevo componente para crear la vista que muestra la frase:

```ps
ng g c chuck/componentes/verfrase
```

Una instancia _Observable_ sólo comienza a publicar valores cuando alguien se suscribe a el usando el método `subscribe()` de la instancia.

Edito el método `ngOnInit` en [src/app/componentes/verfrase/verfrase.component.ts], este método se suscribe al observable y recibe el objeto.

```ts
  ngOnInit() {
    this.apiservice.getFrase().subscribe(frase => (this.frase = frase));
  }
```

La expresión entre paréntesis es una función flecha (_fat arrow_) para simplificar la sintaxis de una función, una expresión equivalente sería algo así (aunque no actualiza la vista con la frase):

```ts
  fraseRecibida(frase: Frase) {
    this.frase = frase;
  }

  ngOnInit() {
    this.apiservice.getFrase().subscribe(this.fraseRecibida);
  }
```

La plantilla del componente se ha mantenido lo más sencilla posible:

```ts
<p>{{ frase.value }}</p>
```

# Código fuente del proyecto

- angular / src / conceptos / http / consumir-servicios-api-rest / [chucknorris-rest-client](https://gitlab.com/soka/angular/tree/master/src/conceptos/http/consumir-servicios-api-rest/chucknorris-rest-client).

# Enlaces externos
