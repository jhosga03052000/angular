[TOC]

# Requisitos para realizar una petición HTTP en Angular

Debemos añadir el módulo `HttpClientModule` al array `imports` de `AppModule` (las líneas marcadas con comentario `//<---`):

```ts
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; // <---


import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule // <----
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

# Llamada HTTP Get en el componente

Para el ejemplo usaré la llamada [https://api.npms.io/v2/search?q=scope:angular](https://api.npms.io/v2/search?q=scope:angular) para obtener el número de paquetes NPM relacionados con Angular, se puede comprobar la respuesta JSON abriendo el enlace en el navegador, dentro de la respuesta JSON queremos obtener el total de paquetes y guardarlo en una nueva variable `totalAngularPackages` de la clase.

[HttpClient](https://angular.io/api/common/http/HttpClient) de @angular/common/[http](https://angular.io/api/common/http) ofrece un cliente HTTP API para facilitar las llamadas. 

En el constructor de la clase se inyecta [HttpClient](https://angular.io/api/common/http/HttpClient).

El método [`Observable`](https://angular.io/guide/observables) se usa para suscribirnos y recibir la respuesta de forma asíncrona, para ver el resultado he añadido un botón HTLM y la función `onSubmit` asociada al evento ([Event binding](https://soka.gitlab.io/angular/conceptos/data-binding/event-binding/event-binding/)).

```ts
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http'; // <----

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'http-get';
  totalAngularPackages; // <---
  url: string = 'https://api.npms.io/v2/search?q=scope:angular';
  error;

  constructor(private http: HttpClient) { } // <---

  ngOnInit() {   // <---
    this.http.get<any>(this.url).subscribe(data => {
       this.totalAngularPackages = data.total;
    },error => this.error = error);
  }
  
  onSubmit() { // <----
    console.log("onSubmit(): this.totalAngularPackages: "+this.totalAngularPackages);
  }
}
```

# Un botón en la vista para mostrar los resultados

```ts
<button type="button" (click)="onSubmit()">Click Me!</button> 
```

# Proyecto

- angular / src / conceptos / http / peticion-http-get / [http-get](https://gitlab.com/soka/angular/tree/master/src/conceptos/http/peticion-http-get/http-get).

# Enlaces internos

- ["Consumir servicios REST"](../consumir-servicios-api-rest/consumir-servicios-api-rest.md).

# Enlaces externos

- morioh.com ["How to send HTTP GET requests from Angular to a backend API"](https://morioh.com/p/1980c46b59bf) - Zara Bryant.
- angular.io ["Observables"](https://angular.io/guide/observables): Observables provide support for passing messages between publishers and subscribers in your application. Observables offer significant benefits over other techniques for event handling, asynchronous programming, and handling multiple values...
- angular.io ["HttpClient"](https://angular.io/guide/http).
- [Ejemplo](https://angular.io/generated/live-examples/http/stackblitz.html) de Angular más completo en Stackblitz.
