[TOC]

![](img/01.PNG)

# Instalando Bootstrap en Angular

El proyecto está creado con la opción de rutas habilitada.

Dentro de la carpeta raíz del proyecto instalamos [**Bootstrap**](https://getbootstrap.com/) y sus dependencias, [jQuery](https://jquery.com/) y [Popper.js](https://popper.js.org/).

```ps
npm install bootstrap jquery popper.js
```

Si todo ha ido correctamente deberíamos ver esta nueva carpeta en nuestro proyecto [node_modules\bootstrap].

# Configuración de los estilos

Ahora debemos incluir los nuevos estilos dentro del fichero "angular.json" para que nuestra aplicación reconozca los cambios que hemos realizado:

```json
            "styles": [
              "node_modules/bootstrap/dist/css/bootstrap.css",
              "src/styles.css"
            ],
```

En el apartado de scripts agregamos también **Bootstrap** y las dependencias que hemos instalado al inicio.

```json
            "scripts": [
              "node_modules/jquery/dist/jquery.slim.min.js",
              "node_modules/popper.js/dist/umd/popper.min.js",
              "node_modules/bootstrap/dist/js/bootstrap.min.js"
            ],
```

Para que los cambios tengan efecto recargamos nuestra aplicación (`ng serve --open`). Con estos sencillos pasos hemos incluido Bootstrap.

# Aplicando los estilos

Editamos [app.component.css] y cambiamos el color de letra del título de nivel uno `<h1>`:

```css
h1 {
  color: aquamarine;
}
```

Podemos hacer esto mismo definiendo el estilo CSS en línea dentro del decorador en [app.component.ts].

Ahora voy a modificar la plantilla para aplicar algunos elementos básicos (contenedores, filas y columnas) de **Bootstrap** para organizar los contenidos en una rejilla ([Grid system · Bootstrap](https://getbootstrap.com/docs/4.0/layout/grid/))

```html
<div class="container">
  <div class="row">
    <div class="col">
      <h1>Hola mundo!</h1>
    </div>
  </div>
</div>
```

Usando el inspector de Chrome podemos observar como aplica los estilos:

![](img/02.PNG)

# Otras alternativas para instalar Bootstrap

## De forma local

Modificamos "angular.json" y añadimos la ruta al CSS.

```ts
"styles": [
  "styles/bootstrap-3.3.7-dist/css/bootstrap.min.css",
  "styles.scss"
],
```

O lo importamos en [src/style.css]:

```ts
@import './styles/bootstrap-3.3.7-dist/css/bootstrap.min.css';
```

## Usando la libreria de Angular ngx-bootstrap

Si no necesitamos los componentes JS de Bootstrap (y a su vez jQuery):

```ps
npm install ngx-bootstrap
```

Una vez instalado debemos añadir los módulos en [app.module.ts].

# Código fuente del ejemplo

- angular / src / conceptos / estilos / bootstrap / [bootstrap-install](https://gitlab.com/soka/angular/tree/master/src/conceptos/estilos/bootstrap/bootstrap-install).

# Enlaces externos

- ["Cómo arrancar un proyecto Angular 6 utilizando Bootstrap 4 y Font ..."](https://medium.com/@smarrerof/c%C3%B3mo-arrancar-un-proyecto-angular-6-utilizando-bootstrap-4-y-font-awesome-4-5322c834c117).
- ["How to Add Bootstrap to an Angular CLI project - Loiane Groner"](https://loiane.com/2017/08/how-to-add-bootstrap-to-an-angular-cli-project/).
- ["Building a Simple Angular 7 App with Bootstrap 4 Styling"](https://codeburst.io/getting-started-with-angular-7-and-bootstrap-4-styling-6011b206080).
