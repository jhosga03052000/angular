[TOC]

# Manejar un evento

Creo un botón en la vista y asocio un evento:

```ts
<button (click)="onSubmit()">Submit</button>
```

Método que maneja evento de pulsación de ratón en el botón:

```ts
  onSubmit() {
    console.log("onSubmit");
  }
```

# Visualización condicional

Ahora aplico la interpolación de cadenas para mostrar un mensaje cuando recibo el evento:

```ts
  onSubmit() {
    console.log("onSubmit");
    this.msg = "Submit clicked!";
  }
```

Usando la directiva condicional [ngIf](https://angular.io/api/common/NgIf) sólo se mostrará el mensaje cuando esté definido. En HTML+CSS es muy común que la etiqueta se cargue pero no este visible, pero sigue ocupando espacio.

```ts
<p *ngIf="msg">Mensaje: {{ msg }}</p>
```

# Evento con parámetros (Template reference variables)

Ahora usaré variables referenciadas a elementos de la plantilla con "#".

```ts
<div class="field">
  <label for="title">Title:</label>
  <input name="title" id="title" #newtitle />
</div>
<div class="field">
  <label for="link">Link:</label>
  <input name="link" id="link" #newlink />
</div>
<button (click)="addData(newtitle.value, newlink.value)">Submit data</button>
```

```ts
  addData(newtitle: string, newlink: string) {
    console.log("newtitle:" + newtitle + ", newlink:" + newlink);
  }
```

# Campo edición de texto

Vista:

```ts
<label for="nombre">Nombre:</label>
<input type="text" (input)="onInputNombre($event)" />
<p>Nombre:{{ nombre }}</p>
```

```ts
nombre: string;
```

```ts
  onInputNombre(event: Event) {
    console.log(event);
    this.nombre = (<HTMLInputElement>event.target).value;
  }
```

# Código fuente

<iframe width="100%" height="600px" src="https://stackblitz.com/edit/evt-binding-basic?embed=1&file=src/app/app.component.ts"></iframe>

# Enlaces externos

- angular.io API > @angular/common [ngIf](https://angular.io/api/common/NgIf).
