[TOC]

# Interpolación de cadenas, del componente al DOM con {{}}

La interpolación de cadenas en **Angular** es el mecanismo para sustituir una expresión por un valor de tipo _string_ en la plantilla (_template_).

Cuando **Angular** se encuentra una expresión entre {{}} en la plantilla lo evalúa y trata de **sustituir el contenido por el valor de la propiedad del componente con el mismo nombre**.

**Este tipo de vinculación o _binding_ es unidireccional**, vincula el valor del elemento a la propiedad del componente.

Para estos ejemplos no vamos a usar estilos CSS, el contenido de la vista lo incrustaremos en el propio componente en TypeScript para ver como funciona de un solo vistazo, borro [app.component.html].

# Ejemplo básico

Este es un ejemplo básico:

```ts
import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  template: `
    <h1>{{ titulo }}</h1>
    <h2>Soy: {{ nombre }}</h2>
  `
})
export class AppComponent {
  titulo = "interpolation";
  nombre: string = "Iker";
}
```

Encerrar el texto entre (`) nos permite dividir el texto en múltiples líneas para facilitar su lectura. Si cambia el valor de la propiedad **Angular** hace lo que corresponde y refresca la vista con los cambios.

Hemos explicitado el tipo de variable como cadena (_string_) pero en realidad se infiere su tipo en la declaración.

También se puede usar la interpolación para definir el valor de una propiedad de una etiqueta.

```ts
<a href="{{ miHref }}">Enlace</a>
```

# Evaluar una expresión

También podemos usar la interpolación para evaluar una expresión:

```ts
<p>La suma de 1 + 1 es {{ 1 + 1 }}.</p>
```

# Invocar un método del componente

También podemos invocar un método del componente:

```ts
<p>Llamada a método retorna: {{ 1 + 1 + dameValor() }}.</p>
```

Y la definición del método:

```ts
  dameValor(): number {
    return 2;
  }
```

# Otros ejemplos

Este ejemplo no es novedoso, usamos un bucle para recorrer los elementos de un array de objetos:

```ts
    <ul>
      <li *ngFor="let cliente of clientes">{{ cliente.nombre }}</li>
    </ul>
```

```ts
clientes = [{ id: 1, nombre: "iker" }, { id: 2, nombre: "asier" }];
```

Negación de un valor lógico (_boolean_):

```ts
<p>{{ !valorLogico }}</p>
```

# Efectos colaterales no permitidos

**Una expresión de una plantilla no debería cambiar ningún estado de la aplicación excepto el valor de la propiedad que estamos tratando** (_binding_ o vinculación de una direccción del componente a la plantilla).

```ts
<p>{{ valorLogico = false }}</p>
```

Produce el siguiente error:

![](img/01.PNG)

Siguiendo el mismo principio no debemos invocar un método que produzca cambios en el componente:

```ts
<p><{{ metodoNoRecomendado() }}/p>
```

```ts
  metodoNoRecomendado() {
   this.valorLogico = false;
  }
```

# ¡Interpolación en acción!

Para ver la interpolación en el momento que se produce voy a cambiar la propiedad del componente cuando se pulse un botón, **Angular de forma automática cambia el contenido en la plantilla cuando cambia el valor de la propiedad**:

```ts
<h2>Soy: {{ nombre }}</h2>
<button (click)="onClickMe()">Click me!</button>
```

```ts
  onClickMe() {
    this.nombre = "Soy yo"; // Activamos interpolación a placer
  }
```

# Código fuente

- angular / src / conceptos / data-binding / [interpolation](https://gitlab.com/soka/angular/tree/master/src/conceptos/data-binding/interpolation).


# Enlaces externos

- angular.io ["Showing component properties with interpolation"](https://angular.io/guide/displaying-data#showing-component-properties-with-interpolation).
- ["Interpolación {{}} en Angular al detalle - Desarrolloweb.com"](https://desarrolloweb.com/articulos/binding-propiedad-vs-interpolacion-strings.html).
- ["Binding a propiedad vs interpolación de strings, en Angular"](https://desarrolloweb.com/articulos/binding-propiedad-vs-interpolacion-strings.html).
- ["¿Cómo usar Expresiones en Interpolación y Property Binding de ..."](https://www.pensemosweb.com/como-usar-expresiones-interpolacion-property-binding/).
