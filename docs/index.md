![](img/Angular_full_color_logo.svg.png)

[TOC]

# Introducción

[**Angular**](<https://es.wikipedia.org/wiki/Angular_(framework)>) es un _framework_ para desarrollar aplicaciones Web, está basado en [**Typescript**](https://es.wikipedia.org/wiki/TypeScript) y apoyado por Google.

[**Typescript**](https://es.wikipedia.org/wiki/TypeScript) es un lenguaje abierto y libre mantenido por MS (Microsoft), extiende la sintaxis de **JavaScript**, esencialmente añade tipado estático de las variables (en JS las variables se declaran sin especificar su tipo `var num = 16;`) y objetos basados en clases que no existen en JS. El código **TypeScript** se traduce a código JavaScript usando un "transpilador". El compilador **TypeScript** se puede instalar (`npm install -g typescript`) como un paquete Node.js (compilación: `tsc helloworld.ts`), los archivos **TypeScript** se guardan con extensión ".ts".

Con **Angular** podemos crear aplicaciones Web conocidas como [**SPA**](https://es.wikipedia.org/wiki/Single-page_application) (_Single Page Application_ o aplicación de página única), normalmente están compuestas por una única página HTML para que la experiencia de usuario al navegar sea más fluida sin necesidad de cargar nuevos elementos cuando navegamos entre diferentes opciones. Una aplicación **Angular** estará compuesta por componentes.

![](img/spa-01.png)

([Fuente](https://adrianalonso.es/desarrollo-web/single-page-app-vs-multi-page-app/))

Basado en un **diseño MVC** ([Modelo Vista Controlador](https://es.wikipedia.org/wiki/Modelo%E2%80%93vista%E2%80%93controlador)), se bada en clases de tipo "Componentes" para realizar el **_Data Binding_** (actualiza las vistas cuando el modelo cambia y a la inversa).

# Enlaces externos

- standardjs.com ["JavaScript Standard Style"](https://standardjs.com/).
- typescriptlang.org ["TypeScript - JavaScript that scales"](https://www.typescriptlang.org/).
